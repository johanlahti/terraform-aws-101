
variable "cidr_range" {
    type        = string
    default     = "10.0.0.0/16"
    description = "The prefix cidr range used in the vpc"
}

variable "instance_count" {
    default     = 3
}

variable "instance_type"{
    type        = string
    default     = "t3.micro"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] # Canonical
}
