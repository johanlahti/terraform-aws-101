terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-north-1"
}


terraform {
  backend "s3" {
    key = "medium-terraform/stage/terraform.tfstate"
    # ...
  }
}
