
resource "aws_vpc" "acebit" {
  cidr_block       = var.cidr_range
  instance_tenancy = "default"

  tags = {
    Name = "acebit"
  }
}

resource "aws_subnet" "web" {
    count       = var.instance_count
    vpc_id      = aws_vpc.acebit.id
    cidr_block  = "10.0.${count.index + 1}.0/24"
    tags = {
        Name    = "subnet${count.index + 1}"
    }
}

resource "aws_security_group" "allow_http_https" {
    name        = "allow http/https"
    description = "Allow http and http inbound"
    vpc_id      = aws_vpc.acebit.id
}

resource "aws_security_group_rule" "permit_443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.allow_http_https.id
}

resource "aws_security_group_rule" "permit_80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.allow_http_https.id
}

resource "aws_network_interface" "int" {
  count                  = var.instance_count
  subnet_id              = aws_subnet.web[count.index].id
  security_groups        = [aws_security_group.allow_http_https.id]
  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_instance" "server" {
  count                  = var.instance_count
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  network_interface {
    network_interface_id = aws_network_interface.int[count.index].id
    device_index         = 0
  }
  tags = {
    Name = "web-${count.index + 1}"
  }
}
